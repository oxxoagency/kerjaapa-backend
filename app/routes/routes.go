package routes

import (
	"github.com/gin-gonic/gin"
	"strings"
	"encoding/base64"
	"kerjaapa_api/app/controllers"
)

func Router() *gin.Engine {
	r := gin.Default()

	r.GET("/status", func(c *gin.Context) {
		c.JSON(200, gin.H {})
	})
	auth := r.Group("/", basicAuth())
	auth.POST("/upload", controllers.UploadFile)
	job := auth.Group("/job") 
	{
		job.GET("/view/:id", controllers.JobDetail)
		job.POST("/list", controllers.JobList)
		job.POST("/store", controllers.JobStore)
	}
	user := auth.Group("/user")
	{
		user.POST("/register_1", controllers.UserRegister1)
		user.POST("/register_2", controllers.UserRegister2)
		user.POST("/register_3", controllers.UserRegister3)
		user.POST("/login", controllers.UserLogin)
		user.POST("/profile", controllers.UserProfile)
	}
	region := auth.Group("/region")
	{
		region.GET("/list", controllers.RegionList)
		// region.GET("/list", controllers.AreaList)
		region.GET("/province_list", controllers.ProvinceList)
		region.GET("/city_list/:province_id", controllers.CityList)
	}
	company := auth.Group("/company")
	{
		company.GET("/list/:uid", controllers.CompanyList)
		company.POST("/store", controllers.CompanyStore)
		company.GET("/:cid", controllers.CompanyDetail)
		company.POST("/edit", controllers.CompanyEdit)
	}
	payment := auth.Group("/payment")
	{
		payment.POST("/snap", controllers.PaymentSnap)
		payment.GET("/paid/:invoice_id", controllers.PaymentPaid)
		payment.POST("/ipaymu", controllers.PaymentIpaymu)
	}
	invoice := auth.Group("/invoice")
	{
		invoice.GET("/:id", controllers.InvoiceDetail)
		invoice.GET("/list", controllers.InvoiceList)
	}
	bookmark := auth.Group("/bookmark")
	{
		bookmark.POST("/store", controllers.BookmarkStore)
		bookmark.GET("/list", controllers.BookmarkList)
	}
	admin := auth.Group("/admin")
	{
		admin.GET("/add_jobs_view", controllers.AddJobsView)
		admin.POST("/job_store", controllers.JobAdminStore)
	}

	return r
}

func basicAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		auth := strings.SplitN(c.Request.Header.Get("Authorization"), " ", 2)

		if len(auth) != 2 || auth[0] != "Basic" {
			respondWithError(401, "Unauthorized", c)
      		return
		}

		payload, _ := base64.StdEncoding.DecodeString(auth[1])
		pair := strings.SplitN(string(payload), ":", 2)

		if len(pair) != 2 || !authenticateUser(pair[0], pair[1]) {
		respondWithError(401, "Unauthorized", c)
		return
		}

		c.Next()
	}
}

func authenticateUser(username, password string) bool {
	// var user models.User
	// // fetch user from database. Here db.Client() is connection to your database. You will need to import your db package above.
	// // This is just for example purpose
	// err := db.Client().Where(models.User{Login: username, Password: password}).FirstOrCreate(&user)
	// if err.Error != nil {
	//   return false
	// }
	if username == "kerjaapa" && password == "21cf7c61786793c4ced864cb4b2fcefcdb543bae0355299cbe74058314cbb7d8" {
		return true
	}
	return false
  }

func respondWithError(code int, message string, c *gin.Context) {
	resp := map[string]interface{}{
		"status": code, 
		"error": message,
	}

	c.JSON(code, resp)
	c.Abort()
}