package util

import (
	"io"
	"crypto/rand"
    "time"
    "log"
    "strconv"
    "net/http"
    "bytes"
    "io/ioutil"
    "encoding/json"

    "kerjaapa_api/app/config"
)

type DM map[string]interface{}

func Req(reqType string, url string, token string, reqBody []byte) (resMap DM) {
	timeout := time.Duration(10 * time.Second)
	client := http.Client { Timeout: timeout, }

	req, err := http.NewRequest(reqType, url, bytes.NewBuffer(reqBody))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", token)

	if err != nil { 
		log.Println(err)
		return DM{} 
	} else {
		resp, err := client.Do(req)
		if err != nil { 
			log.Println(err)
			return DM{} 
		} else {
			defer resp.Body.Close()
			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil { log.Println(err) }

			// log.Println(string(respBody))
			json.Unmarshal([]byte(respBody), &resMap)
			return resMap
		}
	}
}

func GenerateOTP(max int) string {
    b := make([]byte, max)
    n, err := io.ReadAtLeast(rand.Reader, b, max)
    if n != max {
        panic(err)
    }
    for i := 0; i < len(b); i++ {
        b[i] = table[int(b[i])%len(table)]
    }
    return string(b)
}

var table = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

func ParseFilePath(img string, fileType string) string {
    cdn := config.CDN_URL
    if img != "" {
        switch fileType {
        case "company_logo":
            img = cdn + "company/logo/" + img
        case "user_photo":
            img = cdn + "user/photo/" + img
        }
    } else {
        img = config.SITE_URL + "static/image/noimg.jpg"
    }
    return img
}

func Date(year, month, day int) time.Time {
    return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}

func ParseJobDate(strDate string) {
    year, month, day := time.Now().Date()
    
    jobDate, err := time.Parse("2006-01-02 15:04:05", strDate)
    if err != nil { log.Println(err) }

    t1 := Date(year, int(month), day)

    days := t1.Sub(jobDate).Hours() / 24
    log.Println("DIFFERENCE ")
    log.Println(int(days))
}

func ConvertDate(str string) string {
	layoutISO := "2006-01-02 15:04:05"
	// timeStr, _ := time.Parse(time.RFC3339,str);
	timeStr, _ := time.Parse(layoutISO, str)
	timeStr.Format(time.RFC822)

	// layout := "01/02/06"
	out := "02-01-2006 15:04"

	convertTime := timeStr.Format(out)
	//convertTime, _ := time.Parse(layout, timeStr.String())
	log.Println(convertTime)
	return convertTime;
}

func EncryptID(id int) int {
    cryptbase1 := 1234
    cryptbase2 := 4567
    return (id + cryptbase1) ^ cryptbase2
}

func DecryptID(id int) int {
    cryptbase1 := 1234
    cryptbase2 := 4567
    return (id ^ cryptbase2) - cryptbase1
}

// Convert between types
func FloatStr(f float64) (s string) {
	s = strconv.FormatFloat(f, 'f', 0, 64)
	return s
}
func StrFloat(s string) (f float64) {
	f, _ = strconv.ParseFloat(s, 64)
	return f
}
func IntStr(i int) (s string) {
	s = strconv.Itoa(i)
	return s
}
func StrInt(str string) int {
    ints, _ := strconv.Atoi(str)
    return ints
}
func IntFloat(i int) (f float64) {
	return float64(i)
}


// func ParseJobDateOld() {
    // currentTime := time.Now()
    // year := currentTime.Year()
    // day  := currentTime.Day()
    // year := currentTime.Format("2006")
    // month := currentTime.Format("01")
    // day := currentTime.Format("31")
    // log.Println("YEAR: " + year + " MONTH: " + month + " DAY: " + day)
// }