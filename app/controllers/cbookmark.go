package controllers

import (
    "github.com/gin-gonic/gin"
	"log"
	"kerjaapa_api/app/models"
	"kerjaapa_api/app/utils"
)

func BookmarkStore(c *gin.Context) {
	uid := c.Request.Header.Get("Access-Token")
	rb := make(map[string]interface{})
	c.BindJSON(&rb)

	if uid != "" {
		bookmark := &models.Bookmark{
			BookmarkId 	: 0,
			UserId 		: util.StrInt(uid),
			JobId 		: int(rb["job_id"].(float64)),
		}
		err := dbmap.Insert(bookmark)
		if err == nil {
			c.JSON(200, gin.H {
				"status" : 200,
				"user_bookmark_id" : bookmark.BookmarkId,
			})
		} else {
			log.Println(err)
			c.JSON(200, gin.H { "status": 500 })
		}
	} else {
		c.JSON(401, gin.H { "status": 401 })
	}
}

func BookmarkList(c *gin.Context) {
	uid := c.Request.Header.Get("Access-Token")

	if uid != "" {
		var bookmarks []models.UserBookmark
		_, err := dbmap.Select(
			&bookmarks, 
			`select 
				a.user_bookmark_id, a.user_id, a.job_id,
				b.job_title, b.company_id, b.job_desc, b.province_id, b.city_id, b.job_type, b.job_loc, b.view, b.created_at, 
				c.province_name,
				d.city_name 
				from user_bookmark a 
				LEFT JOIN job b ON a.job_id = b.job_id 
				LEFT JOIN data_province c on b.province_id = c.province_id 
				LEFT JOIN data_city d on b.city_id = d.city_id  
				WHERE a.user_id=? AND a.deleted_at is null`, 
			uid)
		if err == nil {
			for i, v := range bookmarks {
				bookmarks[i].JobType = string(v.JobType.([]byte))
				bookmarks[i].JobLoc  = string(v.JobLoc.([]byte))
			}

			c.JSON(200, gin.H {
				"status": 200,
				"data": bookmarks,
			})
		} else {
			log.Println(err)
			c.JSON(200, gin.H { "status": 404 })
		}
	} else {
		c.JSON(401, gin.H { "status": 401 })
	}
}