package controllers

import (
	"log"
	"github.com/gin-gonic/gin"
	"kerjaapa_api/app/models"
	"kerjaapa_api/app/utils"
)

func CompanyList(c *gin.Context) {
	uid := c.Param("uid")
	var companies []models.Company
	resp := make(map[string]interface{})

	query := `select company_id, company_name, 
		IFNULL(company_logo, "") as company_logo, 
		IFNULL(company_about, "") as company_about, 
		IFNULL(company_web, "") as company_web, 
		province_id, city_id from data_company where deleted_at is null and user_id = ?`
	_, err := dbmap.Select(&companies, query, uid)
	if err == nil {
		for i, v := range companies {
			// companies[i].Id = util.EncryptID(v.Id)
			companies[i].LogoPath = util.ParseFilePath(v.Logo, "company_logo")
			companies[i].About 	  = string(v.About.([]byte))
			companies[i].Web 	  = string(v.Web.([]byte))
		}

		resp["status"] = 200
		resp["data"]   = companies
	} else {
		log.Println(err)
		resp["status"] = 404
	}
	c.JSON(200, resp)
}

func CompanyStore(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)
	resp := make(DM)

	if rb["company_name"] != nil && rb["province_id"] != nil && rb["city_id"] != nil {
		body := &models.Company{
			Id 			: 0, 
			Uid 		: rb["user_id"],
			Name 		: rb["company_name"].(string),
			Logo 		: rb["company_logo"].(string),
			Province 	: rb["province_id"],
			City 		: rb["city_id"],
			About 		: rb["company_about"],
			Web 		: rb["company_web"],
		}
		err := validate.Struct(body)

		if err == nil {
			err := dbmap.Insert(body)
			if err == nil {
				resp["status"] = 200
				resp["company_id"] = body.Id
			} else {
				log.Println(err)
				resp["status"] = 500
			}
		} else {
			log.Println(err)
			resp["status"] = 400
		}
	} else {
		resp["status"] = 400
	}
	c.JSON(200, resp)
}

func CompanyEdit(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)
	resp := make(DM)

	if rb["company_name"] != nil && rb["province_id"] != nil && rb["city_id"] != nil {
		body := &models.Company{
			Id 			: int(rb["company_id"].(float64)), 
			Uid 		: rb["user_id"],
			Name 		: rb["company_name"].(string),
			Logo 		: rb["company_logo"].(string),
			Province 	: rb["province_id"],
			City 		: rb["city_id"],
			About 		: rb["company_about"],
			Web 		: rb["company_web"],
		}
		err := validate.Struct(body)

		if err == nil {
			_, err := dbmap.Update(body)
			if err == nil {
				resp["status"] = 200
				resp["company_id"] = body.Id
			} else {
				log.Println(err)
				resp["status"] = 500
			}
		} else {
			log.Println(err)
			resp["status"] = 400
		}
	} else {
		resp["status"] = 400
	}
	c.JSON(200, resp)
}

func CompanyDetail(c *gin.Context) {
	cid := c.Param("cid")
	// icid := util.DecryptID(util.StrInt(cid))
	resp := make(map[string]interface{})

	var company models.Company
	query := `select user_id, company_id, company_name, 
		IFNULL(company_logo,"") as company_logo, 
		IFNULL(company_about, "") as company_about, 
		IFNULL(company_web, "") as company_web,
		province_id, city_id 
		from data_company 
		where company_id=?`
	err := dbmap.SelectOne(&company, query, cid)

	if err == nil {
		// company.Id = util.EncryptID(company.Id)
		company.LogoPath = util.ParseFilePath(company.Logo, "company_logo")
		company.About 	 = string(company.About.([]byte))
		company.Web 	 = string(company.Web.([]byte))
		
		resp["status"] = 200
		resp["data"]   = company
	} else {
		log.Println(err)
		resp["status"] = 404
	}
	c.JSON(200, resp)
}