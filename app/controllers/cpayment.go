package controllers 

import (
    "github.com/midtrans/midtrans-go"
    "github.com/midtrans/midtrans-go/coreapi"
    "github.com/midtrans/midtrans-go/snap"
    // "github.com/midtrans/midtrans-go/iris"
	"github.com/gin-gonic/gin"
	"log"
	// "fmt"
	"kerjaapa_api/app/config"
	"kerjaapa_api/app/utils"
	"kerjaapa_api/app/models"

	"net/url"
	"net/http"
	"strings"
	"io/ioutil"
	"encoding/hex"
	"encoding/json"
	"crypto/hmac"
	"crypto/sha256"
)

var s snap.Client
var mc coreapi.Client

func PaymentSnap(c *gin.Context) {
	if config.ENV == "prod" {
		s.New(config.MIDTRANS_TOKEN, midtrans.Production)
	} else {
		s.New(config.MIDTRANS_TOKEN, midtrans.Sandbox)
	}

	// Create invoice
	rb := make(DM)
	c.BindJSON(&rb)

	data := &models.Invoice {
		UserId 		: int(rb["user_id"].(float64)),
		JobQuota 	: int(rb["quota"].(float64)),
		Amount 		: int(rb["amount"].(float64)),
	}
	e := dbmap.Insert(data)
	if e != nil { log.Println(e) } else {
		// Initiate Snap request
		req := &snap.Request{
			TransactionDetails: midtrans.TransactionDetails{
				OrderID:  util.IntStr(data.InvoiceId),
				GrossAmt: int64(rb["amount"].(float64)),
			}, 
			CreditCard: &snap.CreditCardDetails{
				Secure: true,
			},
		}

		// Request create Snap transaction to Midtrans
		snapResp, _ := s.CreateTransaction(req)
		// fmt.Println("Response :", snapResp)

		// Udate midtrans token to invoice
		_, e := dbmap.Exec("update invoice set token = :token where invoice_id = :invoice_id", DM {
			"token": snapResp.Token,
			"invoice_id": data.InvoiceId,
		})
		if e != nil { log.Println(e) } else {
			c.JSON(200, gin.H {
				"status": 200,
				"token": snapResp.Token,
				"invoice_id": data.InvoiceId,
			})
		}
	}
}

func CheckTransaction(invoice_id string) DM {
	mc.New(config.MIDTRANS_TOKEN, midtrans.Sandbox)
	resp := DM{}
	res, err := mc.CheckTransaction(invoice_id)
	if err != nil {
		// do something on error handle
		return DM {
			"status_code": 404,
		}
	} else {
		resp["status_code"] = res.StatusCode
		// 	"status_message": res.StatusMessage,
		// }
		log.Println("Response: ", res)
	}
	return resp
}

func PaymentPaid(c *gin.Context) {
	if config.ENV == "prod" {
		mc.New(config.MIDTRANS_TOKEN, midtrans.Production)
	} else {
		mc.New(config.MIDTRANS_TOKEN, midtrans.Sandbox)
	}
	uid := c.Request.Header.Get("Access-Token")

	if uid != "" {
		invoice_id := c.Param("invoice_id")

		// Check if invoice payment success
		check, err := mc.CheckTransaction(invoice_id)
		if err != nil { log.Println(err) }

		if check.StatusCode == "200" {
			// Change invoice to paid
			paid_at := check.TransactionTime
			_, e := dbmap.Exec("update invoice set paid=1, paid_at=:paid_at where invoice_id=:invoice_id", DM {
				"paid_at": paid_at,
				"invoice_id": invoice_id,
			})
			if e != nil { log.Println(e) }

			// Get inv detail
			var inv models.Invoice
			e = dbmap.SelectOne(&inv, "select job_quota from invoice where invoice_id=?", invoice_id)
			if e == nil {
				// Add quota to user
				_, e = dbmap.Exec("update data_user set job_quota=job_quota+:quota where user_id=:uid", DM {
					"quota": inv.JobQuota,
					"uid": uid,
				})
			}

			c.JSON(200, gin.H {
				"status": 200,
				"invoice_id": invoice_id,
			})
		} else {
			c.JSON(200, gin.H {
				"status": 404,
			})
		}
	} else {
		c.JSON(401, gin.H {"status": 401})
	}
}

func PaymentIpaymu(c *gin.Context) {
	var ipaymuVa = "0000008112511993"
	var ipaymuKey = "SANDBOX14F3D91A-95BB-41B9-AA30-3274ED3795F4"

	url, _ := url.Parse("https://sandbox.ipaymu.com/api/v2/payment")

	postBody, _ := json.Marshal(map[string]interface{} {
		"product" 		: []string{"Shoes", "Jacket"},
		"qty" 			: []int8{1, 2},
		"price" 		: []float64{350000, 100000},
		"returnUrl" 	: "https://kerjaapa.com",
		"cancelUrl" 	: "https://kerjaapa.com",
		"notifyUrl" 	: "https://kerjaapa.com",
		"referenceId" 	: "TRX1234",
		"buyerName"		: "Test",
		"buyerEmail"	: "a@mail.com",
		"buyerPhone" 	: "12313123",
	})

	// generate signature
	bodyHash := sha256.Sum256([]byte(postBody))
	bodyHashToString := hex.EncodeToString(bodyHash[:])
	stringToSign := "POST:" + ipaymuVa + ":" + strings.ToLower(string(bodyHashToString)) + ":" + ipaymuKey

	h := hmac.New(sha256.New, []byte(ipaymuKey))
	h.Write([]byte(stringToSign))
	signature := hex.EncodeToString(h.Sum(nil))
	// end generate signature

	reqBody := ioutil.NopCloser(strings.NewReader(string(postBody)))

	req := &http.Request{
		Method: "POST",
		URL: url,
		Header: map[string][]string {
			"Content-Type"	: {"application/json"},
			"va" 			: {ipaymuVa},
			"signature"		: {signature},
		},
		Body: reqBody,
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Println(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
	}

	sb := string(body)
	log.Println(sb)
}