package controllers

import (
	"log"
	"net/http"
	"time"
	"strconv"
	"bytes"
	// "os"
	// "fmt"
    "github.com/gin-gonic/gin"
	"github.com/aws/aws-sdk-go/aws"
	// "github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws/credentials"
)

var AccessKeyID string
var SecretAccessKey string
var MyRegion string
var MyBucket string
var filepath string

func ConnectAws() *session.Session {
	AccessKeyID = "AKIA4VQP4S7OJZOABC5Q"
	SecretAccessKey = "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"
	MyRegion = "ap-southeast-3"
   sess, err := session.NewSession(
	 &aws.Config{
	  Region: aws.String(MyRegion),
	  Credentials: credentials.NewStaticCredentials(
	   AccessKeyID,
	   SecretAccessKey,
	   "", // a token will be created when the session it's used.
	  ),
	 })
   if err != nil {
	 panic(err)
	}
   return sess
}

func UploadFile(c *gin.Context) {
	sess := ConnectAws()
	uploader := s3manager.NewUploader(sess)

   	MyBucket = "kerjaapa"
   	file, header, err := c.Request.FormFile("file")
	log.Println(file)
	filename := header.Filename
	size := header.Size
	buffers := make([]byte, size)
  	file.Read(buffers)

	// Get content type
	contentType := http.DetectContentType(buffers)

	dir := ""
	switch(c.Request.FormValue("type")) {
	case "company_logo":
		dir = "company/logo/"
	}
	dir = "company/logo/"

	now := time.Now()      // current local time
	ts := now.Unix() 
	filename = "22/" + strconv.Itoa(int(ts)) + "_" + filename

	// f, err  := os.Open(path)
	if err != nil {
		log.Println(err)
	}

   	// Upload to the s3 bucket
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String("kerjaapa"),
		Key:    aws.String(dir + filename),
		Body:   bytes.NewReader(buffers),
		// ACL: 	aws.String("public-read"),
		ContentType: aws.String(contentType),
	})
	if err != nil {
		log.Println(err)
	}

	c.JSON(200, gin.H{
		"status": 200,
		"dir": dir,
	 	"filepath": filename,
	})
}

func UploadFile3(c *gin.Context) {
	// sess := c.MustGet("sess").(*session.Session)
	sess := ConnectAws()
	uploader := s3manager.NewUploader(sess)
	// uploader := s3manager.New

   	MyBucket = "kerjaapa"
   	file, header, err := c.Request.FormFile("file")
	log.Println(file)
	filename := header.Filename
	size := header.Size
	buffers := make([]byte, size)
  	file.Read(buffers)

	// Save to tmp
	// file2, err := c.FormFile("file")
	// if err != nil { log.Println(err) }
    // path := "tmp/" + file2.Filename
    // err2 := c.SaveUploadedFile(file2, path)
	// if err2 != nil { log.Println(err2) }

	// Get content type
	buffer := make([]byte, 512)

	_, err = file.Read(buffer)
	if err != nil {
		log.Println(err)
	}
	contentType := http.DetectContentType(buffers)

	now := time.Now()      // current local time
	ts := now.Unix() 
	dir := "company/logo/"
	filename = "22/" + strconv.Itoa(int(ts)) + "_" + filename

	// f, err  := os.Open(path)
	if err != nil {
		log.Println(err)
		// return fmt.Errorf("failed to open file %q, %v", filename, err)
	}

   	// Upload to the s3 bucket
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String("kerjaapa"),
		Key:    aws.String(dir + filename),
		Body:   bytes.NewReader(buffers),
		// ACL: 	aws.String("public-read"),
		ContentType: aws.String(contentType),
	})
	if err != nil {
		log.Println(err)
	}

	// Upload
	// svc := s3.New(sess, &aws.Config{Region: aws.String("ap-southeast-3")})
	// up2 := &s3.PutObjectInput{
	// 	Body 	: file,
	// 	Bucket 	: aws.String("kerjaapa"),
	// 	Key 	: aws.String(dir + "Z_" + filename),
	// 	ContentType: aws.String(contentType),
	// }
	// res, err := svc.PutObject(up2)
	// if err != nil { log.Println(err) }
	// log.Println(res)

	c.JSON(200, gin.H{
		"status": 200,
	 	"filepath": filename,
	})
}

func UploadFile2(c *gin.Context) {
    file, err := c.FormFile("file")
	if err != nil { log.Println(err) }
    path := file.Filename
    err2 := c.SaveUploadedFile(file, path)
	if err2 != nil { log.Println(err2) }

	// Upload S3
	// The session the S3 Uploader will use
	// sess := session.Must(session.NewSession())

	// Create an uploader with the session and default options
	// uploader := s3manager.NewUploader(sess)

	// f, err  := os.Open(file)
	// if err != nil {
	// 	return fmt.Errorf("failed to open file %q, %v", filename, err)
	// }

	// // Upload the file to S3.
	// result, err := uploader.Upload(&s3manager.UploadInput{
	// 	Bucket: aws.String(myBucket),
	// 	Key:    aws.String(myString),
	// 	Body:   f,
	// })
	// if err != nil {
	// 	return fmt.Errorf("failed to upload file, %v", err)
	// }
	// fmt.Printf("file uploaded to, %s\n", aws.StringValue(result.Location))

    c.JSON(200, gin.H {
        "status": 200,
    })
}