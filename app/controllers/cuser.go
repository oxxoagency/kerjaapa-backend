package controllers

import (
	"github.com/gin-gonic/gin"
	"log"
	"time"
	"golang.org/x/crypto/bcrypt"
	"encoding/json"

	"kerjaapa_api/app/models"
	"kerjaapa_api/app/config"
	"kerjaapa_api/app/utils"
)

func UserLogin(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)

	query := `select user_id, user_email, user_fullname, user_password, user_photo 
	from data_user 
	where deleted_at is null 
	and user_email=?`
	var user models.User
	err := dbmap.SelectOne(&user, query, rb["user"])
	if err != nil { log.Println(err) }
	log.Println(user)

	err = bcrypt.CompareHashAndPassword([]byte(user.Pass), []byte(rb["pass"].(string)))
	if err == nil {
		var uphoto string
		if user.Photo == nil {
			uphoto = ""
		} else {
			uphoto = *user.Photo
		}
		c.JSON(200, gin.H {
			"status": 200,
			"data": DM {
				"user_id"		: user.Id,
				"user_email"	: user.Email,
				"user_fullname"	: user.Name,
				"user_photo" 	: user.Photo,
				"user_photo_path": util.ParseFilePath(uphoto, "user_photo"),
			},
		})
	} else {
		c.JSON(401, DM {
			"status": 401,
			"msg": "E-mail / Password Salah",
		})
	}
}

func UserRegister1(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)

	resp := gin.H{ "status": 200 }
	err := validate.Var(rb["user_email"], "required,email")
	if err == nil {
		// Check if email registered
		var user models.User
		query := `select user_id from data_user where user_email=? limit 1`
		err   := dbmap.SelectOne(&user, query, rb["user_email"])
		if err != nil {
			otp := util.GenerateOTP(6)
			user_otp := &models.UserOTP{0, rb["user_email"].(string), otp}
			err := dbmap.Insert(user_otp)

			if err == nil {
				// Send email
				body, _ := json.Marshal(DM {
					"client"	: "kerjaapa",
					"from_email": "notif@kerjaapa.com",
					"from_name"	: "KerjaApa Verifikasi",
					"to"		: rb["user_email"],
					"subject"	: "Registrasi Verifikasi OTP",
					"template"	: "kerjaapa_register",
					"data"		: DM {
						"logo"	: "https://kerjaapa.com/static/image/logo_kerjaapa.png",
						"title"	: "Registrasi Verifikasi",
						"otp"	: otp,
					},
				})
				res := util.Req("POST", config.API_EMAIL, config.API_EMAIL_TOKEN, body)
				log.Println(res)

				resp["status"] = 200
				resp["email"]  = res
			} else {
				log.Println(err)
				resp["status"] = 500
			}
			if config.ENV == "dev" { resp["otp"] = otp }
		} else {
			resp = gin.H {
				"status": 409,
				"msg": "E-mail sudah terdaftar",
			}
		}
	} else {
		resp["status"] = 400
		resp["err"] = "Invalid input"
		resp["msg"] = "Format e-mail salah"
		log.Println(err)
	}
	c.JSON(200, resp)
}

func UserRegister2(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)

	// Verify OTP
	var status int64
	var msg string
	if verifyOTP(rb["user_email"].(string), rb["otp"].(string)) {
		status = 200
		msg = "Success"
	} else {
		status = 404
		msg = "Kode OTP Salah"
	}
	c.JSON(200, gin.H {
		"status": status,
		"msg": msg,
	})
}

func UserRegister3(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)
	resp := DM{ "status": 200 }

	if rb["user_email"] != nil && rb["otp"] != nil && rb["user_password"] != nil && rb["user_repassword"] != nil && rb["user_fullname"] != nil {
		if verifyOTP(rb["user_email"].(string), rb["otp"].(string)) {
			body := &models.User{0, rb["user_email"].(string), rb["user_password"].(string), rb["user_fullname"].(string), nil, 0}
			err := validate.Struct(body)
			if err == nil {
				if rb["user_repassword"] != nil && rb["user_password"] == rb["user_repassword"] {
					// Check if email exist
					var user models.User
					query := `select user_id from data_user where user_email=? limit 1`
					err   := dbmap.SelectOne(&user, query, rb["user_email"])

					if err != nil {
						pass, err2 := bcrypt.GenerateFromPassword([]byte(rb["user_password"].(string)), 10)
						user_new := &models.User{0, rb["user_email"].(string), string(pass), rb["user_fullname"].(string), nil, 0}
						if err2 != nil { }

						err := dbmap.Insert(user_new)
						if err == nil {
							resp["status"] = 200
							resp["user_id"] = user_new.Id
						} else {
							log.Println(err)
							resp["status"] = 500
						}
					} else {
						resp["status"] = 409
						resp["msg"] = "E-mail sudah terdaftar"
					}
				} else {
					resp["status"] = 400
					resp["err"] = "Password mismatch"
					resp["msg"] = "Password dan Ulangi Password tidak sama"
				}
			} else {
				log.Println(err)
				resp["status"] = 400
				resp["err"] = "Fill all fields"
			}
		} else {
			resp["status"] = 400
			resp["err"] = "Incorrect OTP"
		}
	} else {
		resp["status"] = 400
		resp["err"] = "Incomplete fields"
	}
	c.JSON(200, resp)
}

func UserProfile(c *gin.Context) {
	rb := make(DM)
	c.BindJSON(&rb)

	resp := make(map[string]interface{})
	uid := rb["user_id"]
	cid := rb["company_id"].(float64)

	var user models.User
	query := "select user_id, user_email, user_fullname, user_photo, job_quota from data_user where user_id=?"
	err := dbmap.SelectOne(&user, query, uid)
	if err == nil {
		respdata := make(map[string]interface{})
		// Check for job posted this month 
		t := time.Now()
		year := t.Year()
		month := t.Month()
		strMonth := util.IntStr(int(month))
		if int(month) < 10 {
			strMonth = "0" + strMonth
		}
		curMonth := util.IntStr(year) + "-" + strMonth
		qj := `select count(job_id) as job_id from job where user_id = :user_id and created_at like "` + curMonth + `%"`
		count, err := dbmap.SelectInt(qj, DM {
			"user_id": rb["user_id"],
		})
		if count == 0 {
			user.Quota += 1
		}

		resp["status"] = 200
		respdata["user"] = user

		// If cid > 0
		if cid > 0 {
			var company models.Company
			// Get company info
			q_company := `select company_id, company_name, IFNULL(company_logo, "") as company_logo, user_id  
			from data_company 
			where user_id = :uid and company_id = :cid`
			err := dbmap.SelectOne(&company, q_company, DM {
				"uid": uid,
				"cid": cid,
			})
			if err == nil {
				respdata["company"] = company
			} else {
				log.Println(err)
			}
		}

		// Get all user companies
		var companies []models.Company
		// q_companies := `select company_id, company_name, IFNULL(company_logo, "") as company_logo, user_id, IFNULL(province_id, "") as province_id, IFNULL(city_id, "") as city_id  
		// from data_company 
		// where user_id = ?`

		queryc := `select company_id, company_name, 
		IFNULL(company_logo, "") as company_logo, 
		IFNULL(company_about, "") as company_about, 
		IFNULL(company_web, "") as company_web, 
		province_id, city_id from data_company where deleted_at is null and user_id = ?`

		_, err = dbmap.Select(&companies, queryc, uid)
		if err == nil {
			if len(companies) > 0 {
				for i, v := range companies {
					companies[i].LogoPath = util.ParseFilePath(v.Logo, "company_logo")
					companies[i].About 	  = string(v.About.([]byte))
					companies[i].Web 	  = string(v.Web.([]byte))
				}
			}
			respdata["companies"] = companies
		} else { log.Println(err) }

		resp["data"] = respdata
	} else {
		resp["status"] = 404
	}
	c.JSON(200, resp)
}

func verifyOTP(email string, otp string) bool {
	query := `select user_otp_id from user_otp where email = :email and otp = :otp limit 1`
	var userOTP models.UserOTP
	err := dbmap.SelectOne(&userOTP, query, DM {
		"email": email,
		"otp"  : otp,
	})
	if err == nil {
		return true
	}
	return false
}