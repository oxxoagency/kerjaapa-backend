package controllers

import (
	"log"
	"time"
	// "golang.org/x/crypto/bcrypt"
	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"

	"kerjaapa_api/app/models"
	"kerjaapa_api/app/utils"
)

func JobList(c *gin.Context) {
	rb := make(map[string] interface{})
	c.BindJSON(&rb)
	
	// hashedPassword, err := bcrypt.GenerateFromPassword([]byte("skhfkjshfkhf136128"), 8)

	// correct := "true"
	// err = bcrypt.CompareHashAndPassword([]byte("$2a$08$2.j3pnZETWM7wYKlC05A3eUR.XDNNldbla5vXE5nxxCu8z8j700aW"), []byte("skhfkjshfkhf136128")) 
	// if err == nil {

	// Get all jobs
	var job []models.JobList
	query := `select a.job_id, a.job_title, a.company_id, a.job_type, a.job_loc, a.view, a.created_at, b.company_name, IFNULL(b.company_logo, "") as company_logo, c.province_name,
	d.city_name 
	from job a 
	left join data_company b on a.company_id = b.company_id 
	left join data_province c on a.province_id = c.province_id 
	left join data_city d on a.city_id = d.city_id  
	where a.deleted_at is null `

	if rb["city_id"] != nil && rb["city_id"] != "" {
		// query += "and a.city_id = :city_id "
		query += `and a.city_id IN ( ` + rb["city_id"].(string) + ` ) `
	}
	if rb["company_id"] != nil && rb["company_id"] != "" {
		query += "and a.company_id = :company_id "
	}
	query += "order by a.created_at desc"

	_, err := dbmap.Select(&job, query, DM {
		"city_id"	: rb["city_id"],
		"company_id": rb["company_id"],
	})
	log.Println(err)

	// Get job count
	count := `select count(job_id) from job where deleted_at is null `
	if rb["city_id"] != nil && rb["city_id"] != "" {
		count += "and city_id = :city_id "
	}
	if rb["company_id"] != nil && rb["company_id"] != "" {
		count += "and company_id = :company_id "
	}
	jobcount, err2 := dbmap.SelectInt(count)
	if err2 != nil { log.Println(err2) }

	if err == nil {
		for i, v := range job {
			job[i].LogoPath = util.ParseFilePath(v.CompanyLogo, "company_logo")
			job[i].JobType  = string(v.JobType.([]byte))
			job[i].JobLoc   = string(v.JobLoc.([]byte))
		}
		c.JSON(200, gin.H {
			"status"	: 200,
			"data"		: job,
			"province"	: rb["province_id"],
			"count" 	: jobcount,
		})
	} else {
		c.JSON(404, gin.H {
			"status": 404,
		})
	}

	// } else {
	// 	log.Println(err)
	// 	c.JSON(401, gin.H {
	// 		"status": 401,
	// 	})
	// }
}

func JobDetail(c *gin.Context) {
	uid := util.StrInt(c.Request.Header.Get("Access-Token"))

	id := c.Param("id")
	var job models.JobList
	query := `select a.job_id, a.job_title, a.job_desc, a.job_type, a.job_loc, a.company_id, a.view, a.created_at, 
	b.company_name, IFNULL(b.company_logo, "") as company_logo, 
	c.province_name, 
	d.city_name,
	IFNULL(e.user_bookmark_id, "") as user_bookmark_id 
	from job a
	left join data_company b on a.company_id = b.company_id 
	left join data_province c on a.province_id = c.province_id 
	left join data_city d on a.city_id = d.city_id 
	left join user_bookmark e on a.job_id = e.job_id AND e.user_id = :uid 
	where a.job_id = :jid LIMIT 1`
	
	err := dbmap.SelectOne(&job, query, DM {
		"uid": uid,
		"jid": id,
	})
	log.Println(err)
	log.Println(job)

	if err == nil {
		// Add counter
		_, err := dbmap.Exec("update job set view = view+1 where job_id = ?", id)
		if err != nil { log.Println(err) }

		// File path
		// var clogo string
		// if job.CompanyLogo == nil {
		// 	clogo = ""
		// } else {
		// 	clogo = *job.CompanyLogo
		// }
		job.LogoPath = util.ParseFilePath(job.CompanyLogo, "company_logo")
		job.JobType  = string(job.JobType.([]byte))
		job.JobLoc   = string(job.JobLoc.([]byte))

		c.JSON(200, gin.H {
			"status" : 200,
			"data"	 : job,
		})
	} else {
		log.Println(err)
		c.JSON(200, gin.H {
			"status" : 404,
		})
	}
}

func JobStore(c *gin.Context) {
	rb := make(map[string]interface{})
	c.BindJSON(&rb)
	resp := DM{ "status": 200 }

	job_title 	:= rb["job_title"]
	user_id 	:= rb["user_id"]
	company_id 	:= rb["company_id"]
	job_desc 	:= rb["job_desc"]
	province_id := rb["province_id"]
	city_id 	:= rb["city_id"]

	if job_title != nil && user_id != nil && company_id != nil && job_desc != nil && province_id != nil && city_id != nil {
		// Check for user quota
		var quota int64
		query := `select job_quota from data_user where user_id=?`
		err   := dbmap.SelectOne(&quota, query, rb["user_id"])
		if err != nil { log.Println(err) }

		// Check if user has posted job this month
		t := time.Now()
		year := t.Year()
		month := t.Month()
		strMonth := util.IntStr(int(month))
		if int(month) < 10 {
			strMonth = "0" + strMonth
		}
		curMonth := util.IntStr(year) + "-" + strMonth
		log.Println(curMonth)
		// var job_posted int64

		qj := `select count(job_id) as job_id from job where user_id = :user_id and created_at like "` + curMonth + `%"`
		count, err := dbmap.SelectInt(qj, DM {
			"user_id": rb["user_id"],
		})
		if err != nil { log.Println(err) }
		// log.Println("JOB COUNT")
		// log.Println(count)

		if quota > 0 || (count == 0) {
			job_new := &models.Job{0, 
				job_title.(string), 
				int64(user_id.(float64)), 
				int64(company_id.(float64)), 
				job_desc.(string), 
				int64(province_id.(float64)), 
				int64(city_id.(float64)),
				int64(rb["job_type"].(float64)),
				int64(rb["job_loc"].(float64)),
				1,
			}
			err := dbmap.Insert(job_new)
			if err == nil {
				if count > 0 {
					// Reduce quota
					_, err := dbmap.Exec("update data_user set job_quota = job_quota-1 where user_id = ?", rb["user_id"])
					if err != nil { log.Println(err) }
				}

				resp["status"] = 200
			} else {
				log.Println(err)
				resp["status"] = 500
			}
		} else {
			resp["status"] = 409
			resp["msg"] = "Anda tidak memiliki quota. Mohon Top Up Quota dulu."
		}
	} else {
		resp["status"] = 400
	}
	c.JSON(200, resp)
}

func JobAdminStore(c *gin.Context) {
	rb := make(map[string]interface{})
	c.BindJSON(&rb)
	resp := DM{ "status": 200 }

	job_title 	:= rb["job_title"]
	user_id 	:= rb["user_id"]
	job_desc 	:= rb["job_desc"]
	province_id := rb["province_id"]
	city_id 	:= rb["city_id"]

	if job_title != nil && user_id != nil && job_desc != nil && province_id != nil && city_id != nil {
		// Insert new company
		company, e := dbmap.Exec(`
			INSERT INTO data_company (user_id, company_name, company_logo, province_id, city_id) 
			VALUES (1, :cname, :clogo, :province, :city)
		`, DM {
			"cname": rb["company_name"],
			"clogo": rb["company_logo"],
			"province": province_id,
			"city": city_id,
		})
		if e == nil {
			cid, e := company.LastInsertId()
			if e != nil { log.Println(e) }

			job_new := &models.Job{0, 
				job_title.(string), 
				int64(user_id.(float64)), 
				int64(cid), 
				job_desc.(string), 
				int64(province_id.(float64)), 
				int64(city_id.(float64)),
				int64(rb["job_type"].(float64)),
				int64(rb["job_loc"].(float64)),
				1,
			}
			err := dbmap.Insert(job_new)
			if err == nil {
				resp["status"] = 200
			} else {
				log.Println(err)
				resp["status"] = 500
			}
		} else {
			log.Println(e)
		}
	} else {
		resp["status"] = 400
	}
	c.JSON(200, resp)
}