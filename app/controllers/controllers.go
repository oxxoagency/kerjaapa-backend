package controllers

import (
	// "net/http"
	"log"
	// "io/ioutil"
	"github.com/go-gorp/gorp"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"github.com/go-playground/validator/v10"

	"kerjaapa_api/app/models"
	"kerjaapa_api/app/config"
)

type DM map[string]interface{}

var (
	validate = initValidator()
	dbmap = initDb()
)

func initDb() *gorp.DbMap {
	db, err := sql.Open("mysql", config.DB_USER + ":" + config.DB_PASS + "@tcp(" + config.DB_HOST + ":" + config.DB_PORT + ")/" + config.DB_DATA)
	checkErr(err, "sql.Open failed")
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	// err = dbmap.CreateTablesIfNotExists()
	// checkErr(err, "Create tables failed")

	dbmap.AddTableWithName(models.User{}, "data_user").SetKeys(true, "Id")
	dbmap.AddTableWithName(models.UserOTP{}, "user_otp").SetKeys(true, "Id")
	dbmap.AddTableWithName(models.Job{}, "job").SetKeys(true, "Id")
	dbmap.AddTableWithName(models.Company{}, "data_company").SetKeys(true, "Id")
	dbmap.AddTableWithName(models.Invoice{}, "invoice").SetKeys(true, "InvoiceId")
	dbmap.AddTableWithName(models.Bookmark{}, "user_bookmark").SetKeys(true, "BookmarkId")
	return dbmap
}

func checkErr(err error, msg string) {
	if err != nil {
		log.Fatalln(msg, err)
	}
}

func initValidator() *validator.Validate {
	v := validator.New()

	// _ = v.RegisterTranslation("required", trans, func(ut ut.Translator) error {
	// 	return ut.Add("required", "{0} is a required field", true) // see universal-translator for details
	// }, func(ut ut.Translator, fe validator.FieldError) string {
	// 	t, _ := ut.T("required", fe.Field())
	// 	return t
	// })

	return v
}