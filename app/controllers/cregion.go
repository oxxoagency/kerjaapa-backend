package controllers

import (
	"log"
	"strconv"
	"strings"
	"github.com/gin-gonic/gin"
	"kerjaapa_api/app/models"
)

func AreaList(c *gin.Context) {
	var data []models.Area
	_, e := dbmap.Select(&data, `
		SELECT region_id, region_name, region_city 
		FROM data_region WHERE deleted_at IS NULL
	`)
	if e == nil {
		c.JSON(200, gin.H {
			"status": 200,
			"data": data,
		})
	} else {
		c.JSON(200, gin.H {
			"status": 404,
		})
	}
}

func RegionList(c *gin.Context) {
	var regions []models.Region
	var provinces []models.Province
	var wherein []string

	query  := `select province_id, province_name from data_province where del=0 order by province_name asc`
	_, err := dbmap.Select(&provinces, query)
	if err == nil {
		// log.Println(provinces)
		for _, v := range provinces {
			// Get cities
			var cities []models.City
			query  := `select city_id, city_name from data_city where del=0 and province_id = ? order by city_name asc`
			_, err := dbmap.Select(&cities, query, v.Id)
			if err == nil {
				// log.Println(cities)
				regions = append(regions, models.Region {
					v,
					cities,
				})

				for _, v := range cities {
					wherein = append(wherein, strconv.Itoa(int(v.Id)))
				}
			} else {
				log.Println(err)
			}
		}

		// Get job counts
		var wherein_str = strings.Join(wherein, ",")
		var job_city_counts []models.JobCityCount
		job_city_results := make(map[int64]interface{})

		q_count := `select city_id, count(job_id) as job_count from job where city_id in (` + wherein_str + `) and deleted_at is null group by city_id`
		_, err := dbmap.Select(&job_city_counts, q_count)
		if err == nil {
			log.Println(job_city_counts)
			for _, v := range job_city_counts {
				job_city_results[v.CityId] = v.JobCount
				// log.Println(v.CityId)
			}
			log.Println(job_city_results)
		} else {
			log.Println(err)
		}

		// Combine arrays
		for i, v := range regions {
			province_count := int64(0)
			for ic, vc := range v.Cities {
				_, exist := job_city_results[vc.Id]
				if exist {
					vc.JobCount = job_city_results[vc.Id].(int64)
					regions[i].Cities[ic] = vc
					province_count += job_city_results[vc.Id].(int64)
				}
			}
			regions[i].ProvinceJobCount = province_count
		}
	} else {
		log.Println(err)
	}
	c.JSON(200, gin.H {
		"status": 200,
		"data": regions,
	})
}

func ProvinceList(c *gin.Context) {
	var provinces []models.Province
	query := `select province_id, province_name from data_province where del=0 order by province_name asc`
	_, err := dbmap.Select(&provinces, query)
	if err != nil {}
	c.JSON(200, gin.H {
		"status": 200,
		"data": provinces,
	})
}

func CityList(c *gin.Context) {
	var cities []models.City
	provinceId := c.Param("province_id")
	query := `select city_id, city_name from data_city where del = 0 and province_id = ? order by city_name asc`
	_, err := dbmap.Select(&cities, query, provinceId)
	if err != nil {}
	c.JSON(200, gin.H {
		"status": 200,
		"data": cities,
	}) 
}