package controllers

import (
    "github.com/gin-gonic/gin"
	"log"
	"kerjaapa_api/app/models"
)

func InvoiceList(c *gin.Context) {
	var inv []models.Invoice
    q := `select invoice_id, user_id, job_quota, invoice_amount, token, paid, created_at 
        from invoice where user_id = :uid 
		ORDER BY invoice_id DESC`
		if c.Query("paid") != "" {
			q += `and paid = :paid`
		}

	_, e := dbmap.Select(&inv, q, DM {
		"uid"	: c.Request.Header.Get("Access-Token"),
		"paid"	: c.Query("paid"),
	})

	if e == nil {
		for i, v := range(inv) {
			inv[i].CreatedAt = string(v.CreatedAt.([]byte))
		}
		c.JSON(200, gin.H {
			"status": 200,
			"data": inv,
		})
	} else {
		log.Println(e)
		c.JSON(200, gin.H {"status":500})
	}
}

func InvoiceDetail(c *gin.Context) {
	var inv models.Invoice
    q := `select invoice_id, user_id, job_quota, invoice_amount, token, paid, created_at 
        from invoice where invoice_id = ?`
    e := dbmap.SelectOne(&inv, q, c.Param("id"))
    if e == nil {
		inv.CreatedAt = string(inv.CreatedAt.([]byte))
		inv.Token     = string(inv.Token.([]byte))
		c.JSON(200, gin.H {
			"status": 200,
			"data"	: inv,
		})
	} else {
		log.Println(e)
		c.JSON(200, gin.H {"status":500})
	}
}