package models

import (
	
)

type Job struct {
	Id 				int64 	`db:"job_id" json:"job_id"`
	Title 			string 	`db:"job_title" json:"job_title"`
	UserId 			int64 	`db:"user_id" json:"user_id"`
	CompanyId 		int64 	`db:"company_id" json:"company_id"`
	Desc 			string 	`db:"job_desc" json:"job_desc"`
	ProvinceID 		int64 	`db:"province_id" json:"province_id"`
	CityID			int64 	`db:"city_id" json:"city_id"`
	JobType 		interface{} 	`db:"job_type" json:"job_type"`
	JobLoc 			interface{} 	`db:"job_loc" json:"job_loc"`
	View 			int64 	`db:"view" json:"view"`
}

type JobList struct {
	Job
	View 			int64 	`db:"view" json:"view"`
	CreatedAt 		string 	`db:"created_at" json:"created_at"`
	ProvinceName	*string `db:"province_name" json:"province_name"`
	CityName		*string `db:"city_name" json:"city_name"`
	CompanyName 	*string `db:"company_name" json:"company_name"`
	CompanyLogo 	string `db:"company_logo" json:"company_logo"`
	LogoPath 		interface{} `json:"company_logo_path"`
	BookmarkId 		*string `db:"user_bookmark_id" json:"user_bookmark_id"`
}
