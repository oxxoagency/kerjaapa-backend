package models

type Bookmark struct {
	BookmarkId 		int 	`db:"user_bookmark_id"`
	UserId 			int 	`db:"user_id"`
	JobId 			int 	`db:"job_id"`
}

type UserBookmark struct {
	Bookmark
	JobList
}

type Area struct {
	RegionId 		int64 	`db:"region_id"`
	RegionName 		string 	`db:"region_name"`
	RegionCity 		string 	`db:"region_city"`
}