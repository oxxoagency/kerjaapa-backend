package models

type User struct {
	Id 		int64 		`db:"user_id, primarykey, autoincrement" json:"user_id"`
	Email 	string 		`db:"user_email" json:"user_email" validate:"required,email"`
	Pass 	string 		`db:"user_password" json:"user_password" validate:"required"`
	Name 	string 		`db:"user_fullname" json:"user_fullname" validate:"required"`
	Photo 	*string 	`db:"user_photo" json:"user_photo"`
	Quota 	int64 		`db:"job_quota" json:"job_quota"`
}

type UserOTP struct {
	Id 		int64 		`db:"user_otp_id, primarykey, autoincrement" json:"user_otp_id"`
	Email 	string 		`db:"email" json:"email"`
	OTP 	string 		`db:"otp" json:"otp"`
}