package models

type Company struct {
    Id 		int 	`db:"company_id" json:"company_id"`
	Uid 	interface{} `db:"user_id" json:"user_id" validate:"required,numeric"`
	Name 	string  `db:"company_name" json:"company_name" validate:"required"`
	Logo 	string 	`db:"company_logo" json:"company_logo"`
	LogoPath interface{} `db:"-" json:"company_logo_path"`
	Province interface{}	`db:"province_id" json:"province_id" validate:"required,numeric"`
	City 	interface{} 	`db:"city_id" json:"city_id" validate:"required,numeric"`
	About 	interface{} 	`db:"company_about" json:"company_about"`
	Web 	interface{} 	`db:"company_web" json:"company_web"`
}

func (c *Company) Modify() {
	c.LogoPath = "adsa"
}