package models

type Invoice struct {
    InvoiceId 		int 		`db:"invoice_id" json:"invoice_id"`
	UserId 			int  		`db:"user_id" json:"user_id"`
	JobQuota 		int 		`db:"job_quota" json:"job_quota"`
	Amount 			int 		`db:"invoice_amount" json:"invoice_amount"`
	Token 			interface{}	`db:"token" json:"token"`
	Paid 			int 		`db:"paid" json:"paid"`
	CreatedAt 		interface{} `db:"created_at" json:"created_at"`
}