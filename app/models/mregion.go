package models

type Region struct {
	Province
	Cities 		[]City 	`json:"cities"`
}

type JobCityCount struct {
	CityId 		int64 	`db:"city_id" json:"city_id"`
	JobCount 	int64 	`db:"job_count" json:"job_count"`
}