package models

type City struct {
	Id 		int64 	`db:"city_id, primarykey, autoincrement" json:"city_id"`
	Name 	string 	`db:"city_name" json:"city_name"`
	JobCount int64 	`json:"job_count"`
}