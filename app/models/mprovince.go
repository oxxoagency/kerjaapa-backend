package models

type Province struct {
	Id 		int64 	`db:"province_id, primarykey, autoincrement" json:"province_id"`
	Name 	string 	`db:"province_name" json:"province_name"`
	ProvinceJobCount 	int64 	`json:"province_job_count"`
}