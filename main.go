package main

import (
	// "net/http"
	// "github.com/gin-gonic/gin"
	
	// "kerjaapa_api/app/controllers"
	"kerjaapa_api/app/routes"
)

func main() {
	r := routes.Router()
	r.Run(":8184")
}